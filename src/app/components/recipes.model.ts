export interface Recipes {
  id: number,
  name: string
  ingredients: {ingredient:string},
  tags: {tag: string},
  time: string,
  price: string,
  link: string
}
