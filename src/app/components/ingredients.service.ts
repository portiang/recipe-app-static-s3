import { AuthService } from './../services/auth.service';
import { Ingredients } from './ingredients.model';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class IngredientsService {
  private ingredientUrl =
    'https://api.staging.cont7.link/api/recipe/ingredients/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private http: HttpClient,
    public authService: AuthService,
    private messageService: MessageService
  ) {}

  getIngredients(): Observable<Ingredients[]> {
    return this.http.get<Ingredients[]>(this.ingredientUrl).pipe(
      tap((_) => this.log('fetched ingredients')),
      catchError(this.handleError<Ingredients[]>('getIngredients', []))
    );
  }

  addIngredient(ingredient: Ingredients): Observable<Ingredients> {
    return this.http
      .post<Ingredients>(this.ingredientUrl, ingredient, this.httpOptions)
      .pipe(
        tap((newIngredient: Ingredients) =>
          this.log(`added ingredient w/ id=${newIngredient.id}`)
        ),
        catchError(this.handleError<Ingredients>('addIngredient'))
      );
  }

  deleteIngredient(id: number): Observable<Ingredients> {
    const url = `${this.ingredientUrl}/${id}`;

    return this.http.delete<Ingredients>(url, this.httpOptions).pipe(
      tap((_) => this.log(`deleted Ingredient id=${id}`)),
      catchError(this.handleError<Ingredients>('deleteIngredient'))
    );
  }

  updateIngredient(ingredient: Ingredients): Observable<any> {
    return this.http.put(this.ingredientUrl, ingredient, this.httpOptions).pipe(
      tap((_) => this.log(`updated hero id=${ingredient.id}`)),
      catchError(this.handleError<any>('updateIngredient'))
    );
  }

  /*
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`IngredientsService: ${message}`);
  }
}
