import { CreateIngredientComponent } from './components/create-ingredient/create-ingredient.component';
import { CreateRecipeComponent } from './components/create-recipe/create-recipe.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { RecipeComponent } from './components/recipe/recipe.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'recipe', component: RecipeComponent, canActivate: [AuthGuard]},
  {path: 'create/recipe', component: CreateRecipeComponent, canActivate: [AuthGuard]},
  {path: 'create/ingredient', component: CreateIngredientComponent, canActivate: [AuthGuard]},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
