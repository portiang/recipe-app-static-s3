import { AuthService } from './services/auth.service';
import { IngredientsService } from './components/ingredients.service';
import { RecipesService } from './components/recipes.service';
import { AuthInterceptor } from './services/authconfig.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RecipeComponent } from './components/recipe/recipe.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { CreateRecipeComponent } from './components/create-recipe/create-recipe.component';
import { CreateIngredientComponent } from './components/create-ingredient/create-ingredient.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RecipeComponent,
    LoginComponent,
    CreateRecipeComponent,
    CreateIngredientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    RecipesService,IngredientsService, AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
